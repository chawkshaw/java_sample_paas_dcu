<!DOCTYPE HTML>
<html>
<head>
<title>Hello World Java Application</title>
<%!
String myTitle="Welcome to Java DCU Application";
String message = "This is a sample java app deployed to tomcat";
%>


</head>
<body>
<br/><br/><br/><br/>
<h1 style="margin-left:6em;"> <%= myTitle%> </h1>
<h2 style="margin-top:6cm;"> <%= message%> </h2>
<h3><%= new java.util.Date() %></h3>
<img src="youhallnotpaas.png"/> 
<div style="font-family: Helvetica, Arial, sans-serif; font-size:14pt;">
<table border="1">
<tr style="background-color: #E5EECC;">
    <th>Param</th>
    <th>Value</th>
</tr>
<tr>
   <td><b>Request Method</b></td><td> <%= request.getMethod() %></td>
</tr>
<tr>
   <td><b>Locale</b></td><td><%= request.getLocale() %></td>
</tr>
<tr>
   <td><b>Server name</b></td><td><%= request.getServerName() %></td>
</tr>
<tr>
   <td><b>Server port</b></td><td><%= request.getServerPort() %></td>
</tr>
<tr>
   <td><b>URI</b></td><td><%= request.getRequestURI() %></td>
</tr>
<tr>
   <td><b>Protocol</b></td><td> <%= request.getProtocol() %>
</tr>
<tr>
   <td><b>Servlet path</b></td><td> <%= request.getServletPath() %></td>
</tr>
<tr>
   <td><b>Remote address</b></td><td><%= request.getRemoteAddr() %></td>
</tr>
<tr>
   <td><b>Remote host</b></td><td><%= request.getRemoteHost() %></td>
</tr>

</table>
</font></div>
</body>
</html>